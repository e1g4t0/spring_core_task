package repository;
import baza.etoBaaaza;
import domain.Trainee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.*;

@Repository
public class TraineeRepository implements GymRepository<Trainee> {
    @Autowired
    private etoBaaaza etoBaza;
    private Map<Long, Trainee> trainees;
    private Map<String, Long> usernames;

    @Override
    public Trainee save(Trainee trainee) {

        trainees.put(etoBaza.getTraineeSequence(), trainee);
        etoBaza.incerementTraineeSequence();

        return trainee;
    }

    public Trainee createTraineeBuUserId(Long userId, String address, Date dateOfBirth){

        Trainee trainee = new Trainee(etoBaza.getTraineeSequence(), dateOfBirth, address, userId);
        trainees.put(etoBaza.getTraineeSequence(), trainee);

        etoBaza.incerementTraineeSequence();
        return trainee;
    }

    @Override
    public Trainee findById(Long id) {
        return trainees.get(id);
    }

    public Trainee findByUsername(String username){

        List<Trainee> traineesAsCollection = new ArrayList<>(trainees.values());
        Long traineeIdUserId = usernames.get(username);
        for (Trainee trainee :
                traineesAsCollection) {
            if(trainee.getUserId().equals(traineeIdUserId)){
                return trainee;
            }
        }

        return null;
    }

    @Override
    public Trainee delete(Long id) {
        return trainees.remove(id);
    }

    @PostConstruct
    private void getTraineetable(){
        trainees = etoBaza.getTrainees();
        usernames = etoBaza.getUsernames();
    }
}

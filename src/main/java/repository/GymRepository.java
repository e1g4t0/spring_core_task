package repository;

public interface GymRepository<T> {
    public T save(T t);
    public T findById(Long id);
    public T delete(Long id);
}

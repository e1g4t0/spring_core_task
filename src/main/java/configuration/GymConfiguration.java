package configuration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"baza", "repository", "service", "facade"})
public class GymConfiguration {

}

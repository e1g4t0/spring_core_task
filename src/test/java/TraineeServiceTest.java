import baza.etoBaaaza;
import configuration.GymConfiguration;
import domain.Trainee;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;
import repository.TraineeRepository;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@RunWith(SpringRunner.class)
@SpringJUnitConfig(GymConfiguration.class)
public class TraineeServiceTest {
    @Autowired
    private TraineeRepository traineeService;
    @Autowired
    private etoBaaaza etoBaza;

    @Test
    public void testSaveTrainee() {
        // Arrange
        Trainee trainee = new Trainee(2L, new Date(), "Test Address", 100L);
        traineeService.save(trainee);
        assertEquals(trainee, etoBaza.getTrainees().get(2L));
    }

    @Test
    public void testCreateTraineeByUserId() {
        Trainee trainee = traineeService.createTraineeBuUserId(3L, "someWhereInAstana", new Date());
        assertEquals(trainee, etoBaza.getTrainees().get(2L));
    }

    @Test
    public void testFindById() {

        Trainee trainee = etoBaza.getTrainees().get(0L);
        assertEquals(trainee, traineeService.findById(0L));
    }

    @Test
    public void testFindByUsername() {
        // Arrange
        String username = "John.Doe";
        Trainee trainee = traineeService.findByUsername(username);
        assertEquals(trainee, etoBaza.getTrainees().get(0L));
    }

    @Test
    public void testDelete() {

        traineeService.delete(0L);

        assertNull(etoBaza.getTrainees().get(0L));
    }
}

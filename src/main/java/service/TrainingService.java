package service;

import domain.Training;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.TrainingRepository;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class TrainingService {
    @Autowired
    private TrainingRepository trainingRepository;
    DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");

    public void saveTraining(Training training){
        trainingRepository.save(training);
    }

    public Training createTraining(Long traineeId, Long trainerId, String trainingName,
                                   Long trainingtype,String date, Double trainingDuration){
        return trainingRepository.createTrainingByTraineeIdAndTrainerId(traineeId, trainerId,
                trainingName, trainingtype, parseDate(date), trainingDuration);
    }

    public Training getTrainingById(Long id){
        return trainingRepository.findById(id);
    }

    private Date parseDate(String dateOfBirth) {

        try {
            return dateFormat.parse(dateOfBirth);
        } catch (ParseException e){
            e.printStackTrace();
        }
        return null;
    }
}

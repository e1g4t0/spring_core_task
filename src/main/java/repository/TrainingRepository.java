package repository;

import baza.etoBaaaza;
import domain.Training;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.Map;

@Repository
public class TrainingRepository implements GymRepository<Training>{
    @Autowired
    private etoBaaaza etoBaza;
    private Map<Long, Training> trainings;
    @Override
    public Training save(Training training) {
        trainings.put(etoBaza.getTrainingSequence(), training);
        etoBaza.incerementTrainingSequence();
        return training;
    }

    public Training createTrainingByTraineeIdAndTrainerId(Long traineeId, Long trainerId, String trainingName,
                                                                      Long trainingtype, Date date, Double trainingDuration){

        Training training = new Training(etoBaza.getTrainingSequence(), trainerId, traineeId,
                trainingName, trainingtype, date, trainingDuration);
        trainings.put(etoBaza.getTrainingSequence(), training);
        etoBaza.incerementTrainerSequence();
        return training;
    }

    @Override
    public Training findById(Long id) {
        return trainings.get(id);
    }

    @Override
    public Training delete(Long id) {
        return trainings.remove(id);
    }

    @PostConstruct
    private void getTrainingtable(){
        trainings = etoBaza.getTrainings();
    }
}

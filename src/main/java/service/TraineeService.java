package service;

import domain.Trainee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.TraineeRepository;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class TraineeService {
    @Autowired
    private TraineeRepository traineeRepository;
    DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");

    public Trainee save(Trainee trainee){
        return traineeRepository.save(trainee);
    }
    public Trainee createTraineeBuUserId(Long userId, String address, String dateOfBirth){
        return traineeRepository.createTraineeBuUserId(userId, address, parseDate(dateOfBirth));
    }

    public Trainee findById(Long id){
        return traineeRepository.findById(id);
    }

    public Trainee findByUsername(String username){
        return traineeRepository.findByUsername(username);
    }

    public Trainee deleteTraineeById(Long id){
        return traineeRepository.delete(id);
    }

    private Date parseDate(String dateOfBirth) {

        try {
            return dateFormat.parse(dateOfBirth);
        } catch (ParseException e){
            e.printStackTrace();
        }
        return null;
    }

}

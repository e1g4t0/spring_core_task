package service;

import domain.Trainer;
import domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.TrainerRepository;

@Service
public class TrainerService {
    @Autowired
    private TrainerRepository trainerRepository;

    public Trainer createTrainerByUserId(Long userId, String specialization){

        return trainerRepository.createTrainerByUserId(userId, specialization);
    }

    public Trainer findByUsername(String username){
        return trainerRepository.findByUsername(username);
    }
}

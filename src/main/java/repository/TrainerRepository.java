package repository;

import baza.etoBaaaza;
import domain.Trainee;
import domain.Trainer;
import domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TrainerRepository implements GymRepository<Trainer>{
    @Autowired
    private etoBaaaza etoBaza;
    private Map<Long, Trainer> trainers;
    private Map<String, Long> usernames;
    @Override
    public Trainer save(Trainer trainer) {
        trainers.put(etoBaza.getTrainerSequence(), trainer);
        etoBaza.incerementTrainerSequence();
        return trainer;
    }

    public Trainer createTrainerByUserId(Long userId, String specialization){

        Trainer trainer = new Trainer(etoBaza.getTrainerSequence(), specialization, userId);
        trainers.put(etoBaza.getTrainerSequence(), trainer);
        etoBaza.incerementTrainerSequence();
        return trainer;
    }
    @Override
    public Trainer findById(Long id) {
        return trainers.get(id);
    }
    public Trainer findByUsername(String username){
        List<Trainer> trainersAsCollection = new ArrayList<>(trainers.values());
        Long trainerIdUserId = usernames.get(username);
        for (Trainer trainer :
                trainersAsCollection) {
            if(trainer.getUserId().equals(trainerIdUserId)){
                return trainer;
            }
        }

        return null;
    }
    @Override
    public Trainer delete(Long id) {
        return trainers.remove(id);
    }

    @PostConstruct
    private void getTrainerTable(){
        System.out.println("fsdfsdffsd");
        trainers = etoBaza.getTrainers();
        usernames = etoBaza.getUsernames();
    }
}

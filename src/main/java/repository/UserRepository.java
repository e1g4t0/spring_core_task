package repository;

import baza.etoBaaaza;
import domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Random;

@Repository
public class UserRepository implements GymRepository<User>{
    @Autowired
    private etoBaaaza etoBaza;
    private Map<Long, User> users;
    private Map<String, Long> usernames;
    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    @Override
    public User save(User user) {

        users.put(etoBaza.getUserSequence(), user);
        usernames.put(user.getUsername(), user.getId());
        etoBaza.incerementUserSequence();

        return user;
    }

    public User createUserByFirstNameAndLastName(String firstName, String lastName) {

        String username = generateUsername(firstName, lastName);
        String password = generatePassword();
        Long userId = etoBaza.getUserSequence();

        User user = new User(userId, firstName, lastName, username, password, true);

        users.put(userId, user);
        usernames.put(username, userId);
        etoBaza.incerementUserSequence();

        return user;
    }

    @Override
    public User findById(Long id) {
        return users.get(id);
    }



    @Override
    public User delete(Long id) {
        return users.remove(id);
    }

    @PostConstruct
    private void getUserTable(){
        users = etoBaza.getUsers();
        usernames = etoBaza.getUsernames();
    }

    private String generateUsername(String firstName, String lastName){

        StringBuilder generatedUsername = new StringBuilder();
        generatedUsername.append(firstName);
        generatedUsername.append(".");
        generatedUsername.append(lastName);

        long countUsername = 1;
        StringBuilder generatedUsernameDrop = new StringBuilder(generatedUsername);

        while(usernames.containsKey(String.valueOf(generatedUsernameDrop))){
            countUsername++;
            generatedUsernameDrop = new StringBuilder(generatedUsername);
            generatedUsernameDrop.append(countUsername);
        }

        return String.valueOf(generatedUsernameDrop);
    }

    private String generatePassword(){
        StringBuilder password = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            int randomIndex = random.nextInt(CHARACTERS.length());
            char randomChar = CHARACTERS.charAt(randomIndex);
            password.append(randomChar);
        }

        return String.valueOf(password);
    }
}

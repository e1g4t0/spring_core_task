package facade;

import baza.etoBaaaza;
import domain.Trainee;
import domain.Trainer;
import domain.Training;
import domain.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import service.TraineeService;
import service.TrainerService;
import service.TrainingService;
import service.UserService;

import java.text.ParseException;
import java.util.Date;
@Component
public class GymFacade {
    private static final Logger logger = LogManager.getLogger(GymFacade.class);
    private TraineeService traineeService;
    private TrainerService trainerService;
    private TrainingService trainingService;
    private UserService userService;
    @Autowired
    public GymFacade(TraineeService traineeService, TrainerService trainerService, TrainingService trainingService, UserService userService) {
        this.traineeService = traineeService;
        this.trainerService = trainerService;
        this.trainingService = trainingService;
        this.userService = userService;
    }

    public Training bookATrainingSlot(String traineeUsername, String trainerUsername, String trainingName,
                                      Long trainingtype,String date, Double trainingDuration){

        Trainee trainee = traineeService.findByUsername(traineeUsername);
        Trainer trainer = trainerService.findByUsername(trainerUsername);

        Training training = trainingService.createTraining(trainee.getId(), trainer.getId(), trainingName,
                trainingtype, date, trainingDuration);

        logger.info("Training has been booked");
        return training;
    }

    public Trainer registerTrainer(String firstName, String lastName, String specialization){

        User user = userService.createUserByFirstNameAndLastName(firstName, lastName);
        Trainer trainer = trainerService.createTrainerByUserId(user.getId(), specialization);

        logger.info("Trainer " + user.getUsername() +  " has been registered");
        return trainer;
    }
    public Trainee registerTrainee(String firstName, String lastName, String dateOfBirth, String address){

        User user = userService.createUserByFirstNameAndLastName(firstName, lastName);

        Trainee trainee = traineeService.createTraineeBuUserId(user.getId(), address, dateOfBirth);
        logger.info("Trainee " + user.getUsername() +  " has been registered");

        return trainee;
    }
}

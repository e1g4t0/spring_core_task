package service;

import domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.UserRepository;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User createUserByFirstNameAndLastName(String firstName, String lastName){
        return userRepository.createUserByFirstNameAndLastName(firstName, lastName);
    }

}

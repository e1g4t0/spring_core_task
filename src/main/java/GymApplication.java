import configuration.GymConfiguration;
import facade.GymFacade;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import baza.etoBaaaza;
public class GymApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(GymConfiguration.class);

        GymFacade facade = applicationContext.getBean(GymFacade.class);
        facade.registerTrainee("John", "Doe", "07-07-2013", "Astana");
        facade.registerTrainee("John", "Doe", "07-07-2013", "Astana");
        facade.registerTrainee("John", "Doe", "07-07-2013", "Astana");
        facade.registerTrainer("John", "Doe", "Nice specialization");
        facade.registerTrainer("John", "Doe", "");
        facade.bookATrainingSlot("John.Doe", "Ricardo.Milos", "leg session", 0L,
                "07-09-2023", 2.50);

        etoBaaaza baaaza = applicationContext.getBean(etoBaaaza.class);
        System.out.println(baaaza.getUsers());
        System.out.println(baaaza.getTrainers());
        System.out.println(baaaza.getTrainings());
    }
}

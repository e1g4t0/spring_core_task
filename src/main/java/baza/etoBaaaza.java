package baza;

import domain.Trainee;
import domain.Trainer;
import domain.Training;
import domain.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class etoBaaaza {
    private static final Logger logger = LogManager.getLogger(etoBaaaza.class);
    private Map<Long, User> users = new HashMap<>();
    private Map<Long, Trainee> trainees = new HashMap<>();
    private Map<Long, Trainer> trainers = new HashMap<>();
    private Map<Long, Training> trainings = new HashMap<>();
    private Map<String, Long> usernames = new HashMap<>();
    private Long userSequence;
    private Long traineeSequence;
    private Long trainerSequence;
    private Long trainingSequence;

    @PostConstruct
    private void initBaza(){
        logger.info("Initializing database");
        userSequence = 0L;
        traineeSequence = 0L;
        trainerSequence = 0L;
        trainingSequence = 0L;

        String usersPath = "";
        String traineesPath = "";
        String trainersPath = "";
        String trainingsPath = "";

        try (InputStream input = new FileInputStream("C:\\Users\\admin\\Desktop\\epamLab\\Spring Core\\gymCRMSystem\\src\\main\\resources\\application.properties")) {

            Properties prop = new Properties();
            prop.load(input);

            usersPath = prop.getProperty("db.users");
            traineesPath = prop.getProperty("db.trainees");
            trainersPath = prop.getProperty("db.trainers");
            trainingsPath = prop.getProperty("db.trainings");

        } catch (IOException e) {
            //loggerError.error("Invalid property");
            logger.error("Invalid property");
            e.printStackTrace();
        }

        JSONParser jsonParser = new JSONParser();
        System.out.println(usersPath);
        try (FileReader reader = new FileReader(usersPath))
        {
            Object obj = jsonParser.parse(reader);

            JSONArray userList = (JSONArray) obj;
            for (Object o : userList) {
                JSONObject userObject = (JSONObject) o;

                User user = new User(userSequence, (String) userObject.get("firstName"), (String) userObject.get("lastName"),
                        (String) userObject.get("username"), (String) userObject.get("password"), (Boolean) userObject.get("isActive"));
                usernames.put((String) userObject.get("username"), userSequence);
                users.put(userSequence, user);
                userSequence++;
            }

        } catch (ParseException | IOException e) {

            //loggerError.error("Invalid JSON input");
            e.printStackTrace();
        }

        DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
        try (FileReader reader = new FileReader(traineesPath))
        {
            Object obj = jsonParser.parse(reader);

            JSONArray traineesList = (JSONArray) obj;
            for (Object o : traineesList) {
                JSONObject userObject = (JSONObject) o;

                Trainee trainee = new Trainee(traineeSequence, dateFormat.parse((String) userObject.get("dateOfBirth")), (String) userObject.get("address"),
                        (Long) userObject.get("userId"));
                trainees.put(traineeSequence, trainee);
                traineeSequence++;
            }

        } catch (ParseException | IOException e) {
            logger.error("Invalid JSON input");
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            //loggerError.error("Invalid Date input");
            throw new RuntimeException(e);
        }

        try (FileReader reader = new FileReader(trainersPath))
        {
            Object obj = jsonParser.parse(reader);

            JSONArray trainersList = (JSONArray) obj;
            for (Object o : trainersList) {
                JSONObject userObject = (JSONObject) o;

                Trainer trainer = new Trainer(trainerSequence, (String) userObject.get("specialization"), (Long) userObject.get("userId"));
                trainers.put(trainerSequence, trainer);
                trainerSequence++;
            }

        } catch (ParseException | IOException e) {
            logger.error("Invalid JSON input");
            e.printStackTrace();
        }

        logger.info("Database has been initialized");
    }
    @PreDestroy
    private void closeBaza(){
        logger.info("Database has been destroyed");
        users.clear();
        trainees.clear();
        trainers.clear();
        trainings.clear();

    }

    public void incerementUserSequence(){
        this.userSequence++;
    }

    public void incerementTraineeSequence(){
        this.traineeSequence++;
    }
    public void incerementTrainerSequence(){
        this.trainerSequence++;
    }
    public void incerementTrainingSequence(){
        this.trainingSequence++;
    }

    public Long getUserSequence() {
        return userSequence;
    }

    public Long getTraineeSequence() {
        return traineeSequence;
    }

    public Long getTrainerSequence() {
        return trainerSequence;
    }

    public Long getTrainingSequence() {
        return trainingSequence;
    }

    public Map<Long, User> getUsers() {
        return users;
    }
    public Map<Long, Trainee> getTrainees() {
        return trainees;
    }

    public void setTrainees(Map<Long, Trainee> trainees) {
        this.trainees = trainees;
    }

    public Map<Long, Trainer> getTrainers() {
        return trainers;
    }

    public void setTrainers(Map<Long, Trainer> trainers) {
        this.trainers = trainers;
    }

    public Map<Long, Training> getTrainings() {
        return trainings;
    }

    public void setTrainings(Map<Long, Training> trainings) {
        this.trainings = trainings;
    }

    public Map<String, Long> getUsernames() {
        return usernames;
    }
}
